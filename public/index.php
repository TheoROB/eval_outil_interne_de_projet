<?php include "includes/connexion.php";?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="style.css">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Gestion de projets</title>
</head>
<body>
    <?php include("includes/header.php") ?>
    <a href="ajout-projet.php"><input class="add" type="button" value="Ajouter un projet"></a>
    <div class="index">
        <table align="center">
            <tr>
                <th>Projets</th>
                <th>Options</th>
            </tr>
            <tr>
            <?php
                $requete = "SELECT * FROM projets";
                $resultat = $conn -> query($requete);
                while($row = $resultat->fetch(PDO::FETCH_ASSOC)){ ?>
                <td><?php echo $row['nom']; ?></td>
                <td>
                    <ul>
                        <a href="description.php?ID=<?php echo $row['id'];?>"><li>Voir</li></a>
                        <a href="edit.php?ID=<?php echo $row['id'];?>"><li>Modifier</li></a>
                        <a href="delete.php?ID=<?php echo $row['id'];?>" onclick="return confirm('Etes vous sur de vouloir supprimer ?')"><li>Supprimer</li></a>
                    </ul>
                </td>
            </tr>
            <?php } ?>
        </table>
</div>
    <?php include("includes/footer.php") ?>
</body>
</html>