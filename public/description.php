<?php include "includes/connexion.php";?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="style.css">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Projets</title>
</head>
<body>
<?php 
    include "includes/header.php";
    if(isset($_GET['ID'])){
        $id = $_GET['ID'];   
        $resultat = $conn->query("SELECT * FROM projets where id=" .$id);
        while($row = $resultat->fetch(PDO::FETCH_ASSOC)){ ?>
            <div class="des">
                <h3><?php echo $row['nom'];?></h3>
                <p><?php echo $row['description'];?></p>
    <?php } ?>
<?php } ?>
<a href="index.php"><button class="retour">Retour</button></a>
</div>
<?php include "includes/footer.php"?>
</body>
</html>