<?php include "includes/connexion.php";?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="style.css">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Gestion de projets</title>
</head>
<body>
   <?php include "includes/header.php";
    if(isset($_GET['ID'])){
        $id = (int)$_GET['ID'];
        $res = $conn->query("SELECT * FROM projets WHERE id = " . $id);
        $row = $res->fetch(PDO::FETCH_ASSOC);
        if(isset($row['description'])) {
    ?>
    <div class="edit">
    <form method="POST">
        <label>Nom du projet:</label>
        <input type="text" name="nom" value="<?php echo $row['nom'];?>">
        <label>Description:</label>
        <textarea class="description" name='description'><?php echo $row['description']; ?></textarea>
        <input type="submit" value="modifier">
    </form>
    <?php } ?>
    <?php } ?>  
    <?php
        if (isset($_POST['nom'])) {

            $des = $_POST['nom'];
            $requete = 'UPDATE projets SET nom="' . $des . '" WHERE id = ' . $id;
            $resultat = $conn->query($requete);
        }
        if (isset($_POST['description'])) {

            $des = $_POST['description'];
            $requete = 'UPDATE projets SET description="' . $des . '" WHERE id = ' . $id;
            $resultat = $conn->query($requete);
            if ($resultat) {
                header("Location:index.php");
            } else {
                echo '<p class="error">Une erreur est survenue</p>';
            }
        }
    ?>
    <a href="index.php"><button class="retour">Retour</button></a>
    </div>
   <?php include "includes/footer.php"?>
</body>
</html>