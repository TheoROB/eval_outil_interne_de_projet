<?php 
    include "includes/connexion.php";
    if(isset($_POST["Enregistrer"])){
        $nom = $_POST['Nom'];
        $description = $_POST['Description'];
        $req = ("INSERT INTO projets (nom, `description`) values ('$nom', '$description')");
        $result = $conn->prepare($req);
        $result ->execute();
        header("location:index.php");
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="style.css">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ajouter un projet</title>
</head>
<body>
    <?php include("includes/header.php") ?>
    <div class="ajout">
    <form method="POST">
        <label>Nom du projet :</label>
        <input type="text" name="Nom" placeholder="Entrez le nom du projet">
        <label>Description :</label>
        <textarea class="description" name="Description" placeholder="Entrez une description"></textarea>
        <button class="enregistrer"type="submit" name="Enregistrer">Enregistrer</button>
    </form>
    <a href="index.php"><button class="retour">Retour</button></a>
    </div>

    <?php include("includes/footer.php") ?>
</body>
</html>